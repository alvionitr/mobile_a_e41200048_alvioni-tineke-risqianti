class ChartModel {
  final String name;
  final String message;
  final String time;
  final String profileUrl;

  ChartModel({required this.name, required this.message, required this.time, required this.profileUrl});
}

final List<ChartModel> items = [
  ChartModel(
      name: 'hilmy',
      message: 'Hello Hilmy',
      time: '12.00',
      profileUrl:
          'https://asset.kompas.com/crops/IjmmRwybxe-4tZrGRN5eMrq_po8=/0x32:700x382/750x500/data/photo/2020/01/09/5e16811892fc7.jpg'),
  ChartModel(
      name: 'riska',
      message: 'hello riska',
      time: '9 march',
      profileUrl:
          'https://assets.bizjournals.com/static/img/potm/marketing/team-success-img.jpg'),
  ChartModel(
      name: 'vita',
      message: 'hello vita',
      time: '10 march',
      profileUrl:
          'https://asset.kompas.com/crops/ployX7cQOqsYqJS2PYvUGv41CaI=/0x0:1000x667/750x500/data/photo/2017/06/22/163146320170622-042902-8311-chef.juna-.atau-.junior-.rorimpandey-.jpg'),
  ChartModel(
      name: 'niva',
      message: 'hello niva',
      time: '12.21',
      profileUrl:
          'https://res.cloudinary.com/dk0z4ums3/image/upload/v1613385369/attached_image/ini-tanda-dan-cara-untuk-berhenti-menjadi-people-pleaser-0-alodokter.jpg'),
  ChartModel(
      name: 'citra',
      message: 'hello citra',
      time: '13.41',
      profileUrl:
          'https://i0.wp.com/post.healthline.com/wp-content/uploads/2021/02/Female_Portrait_1296x728-header-1296x729.jpg?w=1575'),
  ChartModel(
      name: 'adi',
      message: 'hello adi',
      time: '25 january',
      profileUrl:
          'https://assets.promediateknologi.com/crop/0x0:0x0/x/photo/2022/01/22/3410886100.jpg'),
  ChartModel(
      name: 'ashraf',
      message: 'hello ashraf',
      time: '19.00',
      profileUrl:
          'https://pict-a.sindonews.net/dyn/850/pena/news/2020/11/26/700/246960/10-momen-kejujuran-shawn-mendes-dalam-film-in-wonder-fud.jpg'),
  ChartModel(
      name: 'niva',
      message: 'hello niva',
      time: '12.21',
      profileUrl:
          'http://3.bp.blogspot.com/-H5_ULLaD35k/UKaMKn9MYAI/AAAAAAAAADk/C6eWTwt15O4/s320/bn.jpg'),
  ChartModel(
      name: 'citra',
      message: 'hello citra',
      time: '13.41',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'adi',
      message: 'hello adi',
      time: '25 january',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'ashraf',
      message: 'hello ashraf',
      time: '19.00',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'niva',
      message: 'hello niva',
      time: '12.21',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'citra',
      message: 'hello citra',
      time: '13.41',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'adi',
      message: 'hello adi',
      time: '25 january',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'ashraf',
      message: 'hello ashraf',
      time: '19.00',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
];
