import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'dart:html';

void main() {
  runApp(new MaterialApp(
    home: new Home(),
  ));
}

class Home extends StatefulWidget{
  // This widget is the root of your application
  @override 
  _HomeState createState() => _HomeState();
}

// class _HomeState extends State<Home> {
//   @override 
//   Widget build(BuildContext context) {
//     timeDilation = 5.0;
//     return new Scaffold(
//       body: new Container(
//         decoration: new BoxDecoration(
//           gradient: new LinearGradient(
//             begin: FractionalOffset.topCenter,
//             end: FractionalOffset.bottomCenter,
//             colors: [
//               Colors.white,
//               Colors.purpleAccent,
//               Colors.deepPurple
//               ])),
//           ),
//       );
//   }
// }

class _HomeState extends State<Home>{
  @override
  final List<String> gambar = [
    "img/cry.gif",
    "img/disney.gif",
    "img/dora.gif",
    "img/kartun.gif",
    "img/kucing.gif",
    "img/minion.gif",
    "img/patrick.gif",
    "img/sponge.gif"
  ];

   static const Map<String, Color> colors = {
    'cry' : Color(0xff2db569),
    'disney' : Color(0xfff386b8),
    'dora' : Color(0xff45caf5),
    'kartun' : Color(0xffb19ecb),
    'kucing' : Color(0xfff3e48b),
    'minion' : Color(0xfff58e4c),
    'patrick' : Color(0xffffea0e),
    'sponge' : Color(0xffdbe4e9),
  };

  Widget build(BuildContext context) {
    timeDilation = 5.0;
    return new Scaffold(
      body: new Container(
        decoration: new BoxDecoration(
          gradient: new LinearGradient(
            begin: FractionalOffset.topCenter,
            end: FractionalOffset.bottomCenter,
            colors: [Colors.white,Colors.purpleAccent,Colors.deepPurple]),
      ),
      child: new PageView.builder(
        controller: new PageController(viewportFraction: 0.8),
        itemCount: gambar.length,
        itemBuilder:(BuildContext context, int i){
          return new Padding(
            padding: new EdgeInsets.symmetric(horizontal: 5.0, vertical: 50.0),
            child: new Material(
              elevation: 8.0,
              child: new Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  new Hero(
                    tag: gambar[i], 
                    child: new Material(
                      child: new InkWell(
                        child: new Flexible(
                          flex: 1,
                          child: Container(
                            color: colors.values.elementAt(i),
                            child: new Image.asset(
                              "${gambar[i]}",
                              fit: BoxFit.cover,
                            ),
                          ),
                          ),
                          onTap: () => Navigator.of(context).push(
                            new MaterialPageRoute(
                              builder: (BuildContext context) =>
                                new Halamandua(
                                  gambar: gambar[i],
                                  colors:
                                    colors.values.elementAt(i),
                              ))),
                      ),
                    ))
                ],
              ),
            ));
        }),
      ),
    );
  }
}

class Halamandua extends StatefulWidget {
  Halamandua({required this.gambar, required this.colors});
  final String gambar;
  final Color colors;

  @override
  State<Halamandua> createState() => _HalamanduaState();
}

class _HalamanduaState extends State<Halamandua> {
  Color warna = Colors.grey;

  void _pilihannya(Pilihan pilihan) {
    setState(() {
      warna = pilihan.warna;
    });
  }

  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("MOCHI CATS"),
        backgroundColor: Colors.pinkAccent,
        actions: <Widget>[
          new PopupMenuButton<Pilihan>(
            onSelected: _pilihannya,
            itemBuilder: (BuildContext context) {
              return listPilihan.map((Pilihan x) {
                return new PopupMenuItem<Pilihan>(
                  child: new Text(x.teks),
                  value: x,
                );
              }).toList();
            },
          )
        ],
      ),
      body: new Stack(
        children: <Widget>[
          new Container(
            decoration: new BoxDecoration(
                gradient: new RadialGradient(
                    center: Alignment.center,
                    colors: [Colors.purple, Colors.white, warna])),
          ),
          new Center(
            child: new Hero(
                tag: widget.gambar,
                child: new ClipOval(
                    child: new SizedBox(
                        width: 200.0,
                        height: 200.0,
                        child: new Material(
                          child: new InkWell(
                            onTap: () => Navigator.of(context).pop(),
                            child: new Flexible(
                              flex: 1,
                              child: Container(
                                color: widget.colors,
                                child: new Image.asset(
                                  "img/${widget.gambar}",
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                        )))),
          )
        ],
      ),
    );
  }
}

class Pilihan {
  const Pilihan({required this.teks, required this.warna});
  final String teks;
  final Color warna;
}

List<Pilihan> listPilihan = const <Pilihan>[
  const Pilihan(teks: "Red", warna: Colors.red),
  const Pilihan(teks: "Green", warna: Colors.green),
  const Pilihan(teks: "Blue", warna: Colors.blue),
];

